<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Home get
Route::get('/', [HomeController::class, 'view']);

Route::get('/table', [HomeController::class, 'viewtable']);

Route::get('/data-table', [HomeController::class, 'viewdatatable']);

// Auth get
Route::get('/register', [AuthController::class, 'view']);

Route::get('/welcome', function() {
    return view("welcome");
});


// Auth Post
Route::post('/register', [AuthController::class, 'signup']);

// CAST
Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);