<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function view()
    {
        return view('register');
    }

    public function signup(Request $request)
    {
        // dd($request->all());
        $first_name = $request['First_Name'];
        $last_name = $request['Last_Name'];
        
        return view('welcome', ['first_name' => $first_name, 'last_name' => $last_name]);
    }
}
