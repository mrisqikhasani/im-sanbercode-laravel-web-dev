<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function view()
    {
        return view("home");
    }

    public function viewtable()
    {
        return view("table");
    }

    public function viewdatatable()
    {
        return view("datatable");
    }
}
