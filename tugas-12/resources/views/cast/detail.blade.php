@extends('layout.master')

@section('judul')
Halaman Detail {{$cast->nama}}
@endsection

@section('content')
    <h2>{{$cast->nama}}</h2>
    <h3>{{$cast->umur}}</h3>
    <p>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-primary">Kembali</a>
@endsection