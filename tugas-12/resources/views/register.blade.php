<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Html</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/register" method="post">
      @csrf
      <div class="name">
        <label for="FirstName">First name : </label><br /><br />
        <input type="text" name="First Name"/><br />
        <br />
        <label for="LastName">Last Name : </label><br /><br />
        <input type="text" name="Last Name"/>
        <br />
        <br />
      </div>
      <div class="gender">
        <label for="gender">Gender :</label><br /><br />
        <input type="radio" name="gender" id="male" value="male"/>
        <label for="male" name="gender" value="male">Male</label>
        <br />
        <input type="radio" name="gender" id="female" value="female" />
        <label for="female" name="gender" value="female">Female</label>
        <br />
        <input type="radio" name="gender" id="other" value="other"/>
        <label for="other" name="gender" value="other">Other</label>
        <br />
        <br />
      </div>
      <div class="nationality">
        <label for="nationality">Nationality : </label><br /><br />
        <select name="nationality" id="nationality">
            <option name="nationality" value="Indonesian">Indonesian</option>
            <option name="nationality" value="Singaporean">Singaporean</option>
            <option name="nationality" value="Malaysian">Malaysian</option>
            <option name="nationality" value="American">American</option>
        </select>
        <br /> <br />
      </div>
      <div class="language">
        <label for="language">Language Spoken</label><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">
        <label for="language">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="language" value="English">
        <label for="language">English</label> <br>
        <input type="checkbox" name="language" value="Other">
        <label for="language">Other</label> <br><br>
      </div>
      <div class="bio">
        <label for="bio">Bio : </label><br> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
      </div>
      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
