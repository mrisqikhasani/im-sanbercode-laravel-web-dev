<?php 
    require ('animal.php');    
    require ('Frog.php');    
    require('Ape.php');

    // Release 0
    $sheep = new Animal("shaun");
    
    echo "Name: $sheep->nama <br>";
    echo "legs : $sheep->legs<br>";
    echo "cold blodded : $sheep->cold_blodded <br>";
    echo "<br>";

    // Release 1
    $frog = new Frog("buduk");
    echo "Name : $frog->nama <br>";
    echo "legs : $frog->legs <br>";
    echo "cold blodded : $frog->cold_blodded <br>";
    echo "Jump : ". $frog-> jump() . "<br>";
    echo "<br>";


    $sungokong = new Ape("kera sakti");
    $sungokong-> legs = 2;

    echo "Name : $sungokong->nama <br>";
    echo "legs : $sungokong->legs <br>";
    echo "cold blodded : $sungokong->cold_blodded <br>";
    echo "Yell : ". $sungokong->yell()."<br>";
?>  